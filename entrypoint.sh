#!/bin/sh

# Global variables
DIR_CONFIG="/etc/v2ray"
DIR_RUNTIME="/usr/bin"
DIR_TMP="$(mktemp -d)"
SYS_Bit="$(getconf LONG_BIT)"
[[ "$SYS_Bit" == '32' ]] && BitVer='_linux_386.tar.gz'
[[ "$SYS_Bit" == '64' ]] && BitVer='_linux_amd64.tar.gz'



ID=f8a6fe13-c503-4d73-8ec0-b6f9d476fb06
WSPATH=/
PORT=80

# Write V2Ray configuration
cat << EOF > ${DIR_TMP}/heroku.json
{
"log": {
"loglevel": "warning"
},
"levels":{
"1":{
"handshake": 8,
"connIdle": 300,
"uplinkOnly": 8,
"downlinkOnly": 8,
"bufferSize": 512000
}
},
    "inbounds": [{
        "port": ${PORT},
        "protocol": "vless",
        "settings": {
            "clients": [{
                "id": "${ID}",
                "level": 1
            }],
            "decryption":"none"
        },
        "streamSettings": {
            "network": "ws",
            "wsSettings": {
                "path": "${WSPATH}"
            }
        }
    }],
    "outbounds": [{
        "protocol": "freedom"
    }]
}
EOF

# Get V2Ray executable release
curl --retry 10 --retry-max-time 60 -H "Cache-Control: no-cache" -fsSL github.com/v2fly/v2ray-core/releases/latest/download/v2ray-linux-64.zip -o ${DIR_TMP}/v2ray_dist.zip
busybox unzip ${DIR_TMP}/v2ray_dist.zip -d ${DIR_TMP}

# Convert to protobuf format configuration
mkdir -p ${DIR_CONFIG}
cp -f ${DIR_TMP}/heroku.json ${DIR_CONFIG}/config.json

# Install V2Ray
install -m 755 ${DIR_TMP}/v2ray ${DIR_RUNTIME}
rm -rf ${DIR_TMP}




# Run V2Ray
${DIR_RUNTIME}/v2ray -config=${DIR_CONFIG}/config.json
